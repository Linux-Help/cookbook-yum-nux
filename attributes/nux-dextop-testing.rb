default['yum']['nux-dextop-testing']['repositoryid'] = 'nux-dextop-testing'
default['yum']['nux-dextop-testing']['enabled'] = false
default['yum']['nux-dextop-testing']['managed'] = false
default['yum']['nux-dextop-testing']['gpgkey'] = 'http://li.nux.ro/download/nux/RPM-GPG-KEY-nux.ro'
default['yum']['nux-dextop-testing']['gpgcheck'] = true

case node['platform_version'].to_i
when 6
  default['yum']['nux-dextop-testing']['description'] = 'Nux Desktop Testing Packages for Enterprise Linux 6 - $basearch'
  default['yum']['nux-dextop-testing']['baseurl'] = 'http://li.nux.ro/download/nux/dextop-testing/el6/$basearch/'
when 7
  default['yum']['nux-dextop-testing']['description'] = 'Nux Desktop Testing Packages for Enterprise Linux 7 - $basearch'
  default['yum']['nux-dextop-testing']['baseurl'] = 'http://li.nux.ro/download/nux/dextop-testing/el7/$basearch/'
end

