default['yum']['nux-dextop']['repositoryid'] = 'nux-dextop'
default['yum']['nux-dextop']['enabled'] = true
default['yum']['nux-dextop']['managed'] = true
default['yum']['nux-dextop']['gpgkey'] = 'http://li.nux.ro/download/nux/RPM-GPG-KEY-nux.ro'
default['yum']['nux-dextop']['gpgcheck'] = true

case node['platform_version'].to_i
when 6
  default['yum']['nux-dextop']['description'] = 'Nux Desktop Community Packages for Enterprise Linux 6 - $basearch'
  default['yum']['nux-dextop']['baseurl'] = 'http://li.nux.ro/download/nux/dextop/el6/$basearch/ http://mirror.li.nux.ro/li.nux.ro/nux/dextop/el6/$basearch/'
when 7
  default['yum']['nux-dextop']['description'] = 'Nux Desktop Community Packages for Enterprise Linux 7 - $basearch'
  default['yum']['nux-dextop']['baseurl'] = 'http://li.nux.ro/download/nux/dextop/el7/$basearch/ http://mirror.li.nux.ro/li.nux.ro/nux/dextop/el7/$basearch/'
end

